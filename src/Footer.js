import React from 'react';

export default class Footer extends React.Component {
	render(){
		return(
			<div className="FooterContainer topOrBot"><div className="Footer">
				<p className="FooterContent">
				{/*-----------------------------------------------------------------------------------------------*/}
				</p>
				<p className="FooterContent">© 2021 Arrow Nail LLC. All Rights Reserved • Built with <a href='https://reactjs.org/' target="_blank" rel="noreferrer noopener" className="Footerlink">React</a></p>{/*-----------------------------------------------------------------------------------------------*/}
				<p className="FooterContent">Website designed by <a href='https://www.linkedin.com/in/reeceappling' target="_blank" rel="noreferrer noopener" className="Footerlink">Reece Appling</a></p>
			</div></div>
	);}
}