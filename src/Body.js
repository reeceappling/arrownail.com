import React from 'react';
import './Body.css';

export default class BodyContainer extends React.Component {
	constructor(props){
		super(props);
		this.state={page:this.props.page};
		this.changePage=this.changePage.bind(this);
		this.linkClicked = this.props.linkClicked;
		this.props.register(this.changePage);//Registers its page changer to the parent (app)
	}
	changePage(pg){this.setState({page:pg});};
	render(){
		switch(this.state.page){
			case 1: //case for 1, Services
				return(<div className="BodyContainer">
					<div className="BodyContent">
						<h1>SERVICES CONTENT HERE</h1>{/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/}
					</div>
				</div>);
			case 2: //case for 3, Weather API
				return(<div className="BodyContainer">
					<div className="BodyContent">
						<h1>WEATHER API CONTENT HERE</h1>{/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/}
					</div>
				</div>);
			case 3: //case for 2, Contact
				return(<div className="BodyContainer">
					<div className="BodyContent">
						<h1>CONTACT CONTENT HERE</h1>{/*-------------------------------------------------------------------------------------------------------------------------------------------------------------*/}
					</div>
				</div>);
			default:	//case for 0, default, Homepage
				return(<div className="BodyContainer">
					<div className="BodyContent">
						<div className="homeTopWrapper homeWrapper">
							<div className="homeTop">
								<div className="homeEstimatesLinkArea">
									<div><h2>100% Count On</h2></div>
									<div className="homeAreaButtonWrapper">
										<PageLink txt="Free Estimates" pg="Contact" fxn={this.props.linkClicked}/>
									</div>
								</div>
								<div className="homeLogoArea">
									{/*<img src={require("./images/------")}/>---------------------------------------------------------------------------------------------------------------------------------*/}
								</div>
							</div>
						</div>
						<div className="homeBotWrapper homeWrapper">
							<div className="homeBot">
								<div className="homeBotHeader"></div>
								<div className="homeBotContent">
									{/*
									<HomeContent imgSrc={"-------------------REPLACE ME!!!---------------------"} hasButton={true}  reversed={false} headTxt="Show up on time and ready to go."        btnTxt="Get in Touch" linkTo="Contact" txt="We offer an end-to-end client experience that includes communication, budgeting, staffing, on-site organization, and solid, quality work every time." ></HomeContent>
									<HomeContent imgSrc={"-------------------REPLACE ME!!!---------------------"} hasButton={true}  reversed={true}  headTxt="Do fantastic high-quality work for you." btnTxt="Contact Us" linkTo="Contact" txt="We have worked with homeowners and produce top notch work. Call us today and bring our project management skills and extensive experience to your next project." ></HomeContent>
									<HomeContent imgSrc={"-------------------REPLACE ME!!!---------------------"} hasButton={false} reversed={false} headTxt="And work with you for you."              txt="Our past projects include both new construction and repairs. Occupied homes are always welcomed." ></HomeContent>
									*/}
								</div>
							</div>
						</div>
					</div>
				</div>);
		}
	}
}

//========================= COMPONENTS FOR HOME PAGE (0)/(default) =================================================


/* PROPS!!!!
		this.props.imgSrc: source for content image to display (filepath after src/images/)
		this.props.hasButton: (boolean) is a button present
		this.props.reversed: (boolean) is the pair in backwards order
		this.props.headTxt: Header text for this section
		this.props.txt: Paragraph for this section
		this.props.btnTxt: Text to be displayed on the button in this section
		this.props.linkTo: what page (STRING) the button links to
	constructor(props){
		super(props);
	}
	*/
	/*
class HomeContent extends React.Component { //--------------------------------------------------NOT YET TESTED--------------------------------------------------
	
	render(){
		let divs = [[],[]]; 
		//Push all 1st div items to div[0]
		divs[0].push(<img src={require('./images/'+this.props.imgSrc)} alt="" />); //Push image here
		//Push all 2nd div items to div[1]
		divs[1].push(<h2 className="homePairHeader">{this.props.headTxt}</h2>);//Push Header
		divs[1].push(<p className="homePairTxt">{this.props.txt}</p>);//Push Paragraph text
		//If a button is requested, push it to div[1]
		if(this.props.hasButton){divs[1].push(<div className="homeAreaButtonWrapper"><div className="homeAreaButton" onClick={document.getElementById("nav"+this.props.linkTo).click()}><p>{this.props.btnTxt}</p></div></div>);}
		return(<div className="homeContentPair">
			<div>{(this.props.reversed ? divs[1] : divs[0])}</div>
			<div>{(this.props.reversed ? divs[0] : divs[1])}</div>
		</div>);
	}
} */

class PageLink extends React.Component {
	/* Props
		txt = text to display
		pg = page (string) to go to
	*/
	constructor(props){
		super(props);
		this.onClick = this.onClick.bind(this);
	}
	onClick(e){this.props.fxn(this.props.pg);}
	render(){return(<div className="homeAreaButton" onClick={this.onClick}><p>{this.props.txt}</p></div>);}
}

//========================= COMPONENTS FOR SERVICES PAGE (1) =================================================

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//========================= COMPONENTS FOR WEATHER API PAGE (2) =================================================

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//========================= COMPONENTS FOR CONTACT PAGE (3)  =================================================

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
