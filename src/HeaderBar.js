import React from 'react';
import topLogo from './images/ArrowNail-BlueNailWhiteBackRedCircle.png';

export default class HeaderBar extends React.Component {
	constructor(props){
		super(props);
		this.changePage = this.props.bodyChangeFunc;
		this.navBar = React.createRef();
		this.state = {navBarOptions:this.props.navOpts};
	}
	render(){
		return(
			<div className="headerBarWrapper">
				<div className="headerBar topOrBot">
					<div className="headerImageArea headerArea">
						<img src={topLogo} className="header-logo" alt="Arrow Nail logo" />
					</div>
					<div className="headerTitleArea headerArea">
						<svg viewBox="0 0 75 15" className="TitleSVG">
							<text x="0" y="15">Arrow Nail</text>{/*------------------------------------------------------------------NEED TO CHANGE FONT ---------------------------------*/}
						</svg>
					</div>
					<div className="headerTabsArea headerArea">
							<div className="hamburgerOutline">
								<input type="checkbox" id="hamburgerInput"/>
								<div className="burgerOutline"></div>
								<HamburgerIcon></ HamburgerIcon>
								<NavBar bodyChangeFunc={this.changePage} navOpts={this.state.navBarOptions} startPage={this.props.page} ref={this.navBar}/>
							</div>
					</div>
				</div>
			</div>
		);
	}
}

//Navigation bar
class NavBar extends React.Component {
	constructor(props){
		super(props);
		let tempArr = this.props.navOpts;
		this.state = {activeChild:this.props.startPage,navBarOptions:this.props.navOpts};
		this.talkToChildren = tempArr;
		this.changePage = this.props.bodyChangeFunc;
		this.changeTab = this.changeActiveTab.bind(this);
		this.addToMe = this.addToMe.bind(this);
	}
	addToMe(z,fxn){
		let tempArr = this.talkToChildren;
		tempArr[z] = fxn;
		this.talkToChildren=tempArr;
	};
	changeActiveTab(changeTo){//Change active tab
		if(this.state.activeChild!==changeTo){//only change if trying to change to a different tab
			let tmpState = this.state;
			tmpState.activeChild = changeTo;
			this.setState(tmpState);
			for(var j=0;j<this.state.navBarOptions.length;j++){
				this.talkToChildren[j](changeTo);
			}
			this.changePage(changeTo);
			document.getElementById("hamburgerInput").checked = false;
		}
	};
	render(){
		let tabs = [];
		for(var i=0;i<this.state.navBarOptions.length;i++){
			tabs.push(<NavItem name={this.state.navBarOptions[i]} key={i} i={i} active={this.state.activeChild} handleClick={this.changeTab} registerChildren={this.addToMe}/>);
		};
		return (<div className="navBar"><div className="navBarSelectionArea">{tabs}</div></div>);
	}
}

//Navigation item
class NavItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {i:this.props.i,name:this.props.name,active:this.props.active};
		this.handleClick = this.props.handleClick;
		this.tellParent = this.tellParent.bind(this);
		this.changeActive = this.changeActive.bind(this);
		this.props.registerChildren(this.state.i,this.changeActive);
	}
	changeActive(changeTo){
		let tmpState = this.state;
		tmpState.active = changeTo;
		this.setState(tmpState);
	};
	tellParent(e){if(this.state.active!==this.state.i){this.handleClick(this.state.i);}};
	render(){
		let navClasses = "navItem";
		if(this.state.i===this.state.active){navClasses = "navItem navActive";}
		return(
			<div key={this.state.i} className={navClasses} onClick={this.tellParent}>
				<div className="navInner">
					<h1>{this.state.name}</h1>
				</div>
			</div>
		);
	}
}

class HamburgerIcon extends React.Component {
	constructor(props){
		super(props);
		this.state = {active: false};
		this.swap = this.swap.bind(this);
	}
	swap(){this.setState({active:!this.state.active});}
	render(){
		return(
			<div className="hamburgerWrapper">
				<div className="nail"></div>
				<div className="nail"></div>
				<div className="nail"></div>
			</div>
		);
	}
}