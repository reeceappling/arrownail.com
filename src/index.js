import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


import './App.css';
import HeaderBar from './HeaderBar';
import BodyContainer from './Body'
import Footer from './Footer'

const navBarOptions = ["Home","Services","Weather API","Contact"];
let navBarOptsCopy = ["Home","Services","Weather API","Contact"];
 //USE REACT LINKS TO DO STICKY ANCHORS!!!! https://javascript.plainenglish.io/creating-a-hash-anchor-link-effect-with-react-router-a63dcb1a9b0e -----------------------------------------

class App extends React.Component {
	constructor(props){
		super(props);
		let tmpState = {
			currentPage:0
		};
		let hash = window.location.hash;
		if(hash !== ""){
			hash = hash.substr(1);
			if(navBarOptions.includes(hash)){
				tmpState.currentPage = navBarOptions.indexOf(hash);
			}		
		}
		this.state = tmpState;
		this.header = React.createRef();
		this.changePage=this.changePage.bind(this);
		this.changeHeader=this.changeHeader.bind(this);
		this.returnFunc=this.returnFunc.bind(this);
	}
	returnFunc(fxn){this.returnFunc=fxn;};
	changePage(changeTo){
		let tmpState = this.state;
		tmpState.currentPage = changeTo;
		this.setState(tmpState);
		this.returnFunc(changeTo);
	};
	changeHeader(changeTo){
		this.changePage(changeTo);
		this.header.current.navBar.current.changeActiveTab(navBarOptions.indexOf(changeTo));
	};
	render(){
		return (
			<div>
				<HeaderBar bodyChangeFunc={this.changePage} navOpts={navBarOptsCopy} page={this.state.currentPage} ref={this.header}/>
				<BodyContainer register={this.returnFunc} page={this.state.currentPage} linkClicked={this.changeHeader} />
				<Footer />
			</div>
		);
	}
}

ReactDOM.render(
	<React.StrictMode>
		<App></ App>
	</React.StrictMode>,
	document.getElementById('App')
);

// If you want to start measuring performance in your app, pass a function			---------------------------------------
// to log results (for example: reportWebVitals(console.log))						----------------------------------------------------
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals			---------------------------------------

/*
import reportWebVitals from './reportWebVitals';
reportWebVitals();
*/

/*
Totally not me tracking times
11/3 - 	(5) Template and Header
11/4 - 	(6) Hamburger menu and begin body
11/5 - 	2:15-3:00 (0:45) Fix CI/CD Build,
		7:10-8:25 (1:15) Set up pages integration without DNS records, fixed nails on mobile (2)
*/