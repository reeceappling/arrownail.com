# Repository for ArrowNail.com Frontend

## Description

Arrow Nail LLC website, built with React

## Test and Deploy

Deployments of the **main** branch are built automatically with Gitlab's built-in continuous integration, which also pre-tests the built code.

## Support

For support regarding Arrow Nail and its services, contact ------------------------------------------------------------

For support on the code itself, contact @reeceappling

## Roadmap
- [x] Create blank template site
  - [x] Decide SPA vs MPA > SPA + 404
- [x] fix any npm audit vulnerabilities
- [ ] Update repo licensing
  - [ ] Decide on license
  - [ ] Update licensing on all project files, including below in the License section
- [x] Create navigation bar
- [x] Create all pages (blank) as needed
- [ ] Create Contact Page
  - [ ] Create form
  - [ ] Create anti-bot-submission (Prove you are human)
- [ ] Decide on tenative design
- [ ] Create CI/CD build/test/deploy pipeline
- [ ] Create mobile site
  - [ ] Hamburger menu
  - [ ] static stylings
  - [ ] Animations
- [ ] Create desktop site
- [ ] Correct color scheme
- [ ] Change DNS resolution to arrownail.com rather than reeceappling.gitlab.io/arrownail.com
- [ ] SEO Optimization, meta tags, theme color, description, and apple touch icon
  - [ ] Added all relevant meta tags -----------------------------------------------------
  - [ ] Robots.txt in /public
  - [ ] Change theme color in public/index.html
  - [ ] Change description in public/index.html
  - [ ] Change apple touch icon in public/index.html
  - [ ] SEO iterations
    1. [ ] SEO iteration - 01/01/2022 -  - Initial try -----------------------------------------------------
    1. [ ] SEO iteration - 01/01/2022 -  - second try -----------------------------------------------------
- [ ] Create 404 page or redirect
- [ ] Add analytics
- [ ] Iterate on Designs to reach final acceptable design
  1. [x] 01/01/2022 -  - Initial Design ------------------------------------------------------------------
  1. [ ] ----------------------------------------------------------------------------

## Authors and acknowledgment

@reeceappling - Implementing site design and CI/CD

Kyle Johnson - Logo svg ---------------------------------------------------------------------------

Mark Johnson - Consulted for design and iteration ------------------------------------------------------------------------------------

Luke Johnson - Consulted for graphic designs ------------------------------------------------------------------------------------

## Project status

On Hold

## License

Copyright Arrow Nail LLC 2021
